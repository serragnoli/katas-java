package com.fabs.kata.problem5;

import java.util.*;
import java.util.stream.IntStream;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

class App {

    private static final List<Integer> PRIME_NUMBERS = asList(2, 3, 5, 7, 11, 13, 17, 19);

    static int lcmOfRange(int endIncl) {
        List<Integer> rangeFilled = IntStream.rangeClosed(1, endIncl)
                .boxed()
                .collect(toList());

        Map<Integer, Integer> factors = primeFactorsOf(rangeFilled);

        OptionalInt result = factors.entrySet().stream()
                .mapToInt(x -> (int) Math.pow(x.getKey(), x.getValue()))
                .reduce((a, b) -> a * b);

        return result.orElse(0);
    }

    private static Map<Integer, Integer> primeFactorsOf(List<Integer> rangeFilled) {
        final Map<Integer, Integer> primeFactors = new LinkedHashMap<>(rangeFilled.size());

        for (Integer curr : rangeFilled) {
            decomposeFactors(curr, primeFactors);
        }

        return primeFactors;
    }

    private static void decomposeFactors(Integer current, Map<Integer, Integer> primeFactors) {
        final Map<Integer, Integer> factors = new HashMap<>();

        for (Integer primeNumber : PRIME_NUMBERS) {
            while (current % primeNumber == 0) {
                factors.put(primeNumber, factors.getOrDefault(primeNumber, 0) + 1);
                current /= primeNumber;
            }
            if (primeFactors.getOrDefault(primeNumber, 0).compareTo(factors.getOrDefault(primeNumber, 0)) < 0) {
                primeFactors.put(primeNumber, factors.getOrDefault(primeNumber, 0));
            }
        }
    }
}
