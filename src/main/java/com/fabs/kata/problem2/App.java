package com.fabs.kata.problem2;

import java.util.ArrayList;
import java.util.List;

class App {

    static List<Integer> fibonacciValuesUpTo(int terms) {
        List<Integer> result = new ArrayList<>(terms);
        int previousValue = 0;
        int currentValue = 0;

        for (int i = 1; i <= terms; i++) {
            if (i == 1) {
                result.add(1);
                previousValue = 1;
                currentValue = 1;
                continue;
            }

            int newValue = previousValue + currentValue;
            result.add(newValue);
            previousValue = currentValue;
            currentValue = newValue;
        }

        return result;
    }

    static int sumEvensUpTo(int valueCap, int terms) {
        List<Integer> values = fibonacciValuesUpTo(terms);

        return values.stream()
                .filter(x -> x % 2 == 0 && x < valueCap)
                .mapToInt(x -> x)
                .sum();
    }
}
