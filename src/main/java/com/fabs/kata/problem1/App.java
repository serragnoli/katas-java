package com.fabs.kata.problem1;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class App {

    static int sumOf(int cap) {
        List<Integer> multiples = listMultiplesOfThreeAndFiveUpTo(cap);

        return multiples.stream().mapToInt(x -> x).sum();
    }

    static List<Integer> listMultiplesOfThreeAndFiveUpTo(int cap) {
        return IntStream.range(1, cap)
                .filter(x -> (x % 3 == 0 || x % 5 == 0))
                .peek(x -> System.out.print(" " + x))
                .boxed()
                .collect(Collectors.toList());
    }
}
