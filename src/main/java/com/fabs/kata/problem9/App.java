package com.fabs.kata.problem9;

class App {
    static int productOfPythagoreanOf(int sum) {
        for (int a = 1; a <= sum / 3; a++) {
            for (int b = a + 1; b <= sum / 2; b++) {
                int c = sum - a - b;
                boolean isPythagoreanTriplet = Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2);
                if (isPythagoreanTriplet) {
                    System.out.println("a: " + a + " b: " + b + " c: " + c);

                    return (int) (a * b * c);
                }
            }
        }
        return 0;
    }
}
