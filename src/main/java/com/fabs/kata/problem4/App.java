package com.fabs.kata.problem4;

import java.util.*;
import java.util.stream.IntStream;

import static java.util.Comparator.naturalOrder;
import static java.util.stream.Collectors.toList;

class App {
    private static List<Integer> twoDigit = IntStream.rangeClosed(10, 99)
            .boxed()
            .collect(toList());

    private static List<Integer> threeDigit = IntStream.rangeClosed(100, 999)
            .boxed()
            .collect(toList());

    private static Map<Integer, List<Integer>> dictionary = new HashMap<>();

    static {
        dictionary.put(2, twoDigit);
        dictionary.put(3, threeDigit);
    }

    static Optional<Integer> largestPalindromicOf(int factor) {
        final List<Integer> rangeChoice = dictionary.get(factor);

        final Set<Integer> allProducts = new HashSet<>(rangeChoice.size());
        rangeChoice.forEach(x -> rangeChoice.forEach(y -> allProducts.add(x * y)));

        return chooseLargestPalindromic(allProducts);
    }

    private static Optional<Integer> chooseLargestPalindromic(Set<Integer> allProducts) {
        return allProducts.stream()
                .filter(x -> x.toString().equals(new StringBuilder(x.toString()).reverse().toString()))
                .max(naturalOrder());
    }
}
