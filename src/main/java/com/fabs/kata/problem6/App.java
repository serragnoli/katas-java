package com.fabs.kata.problem6;

import java.util.stream.IntStream;

class App {
    static int differenceOfSquaresOfFirst(int naturalNumbers) {
        int sumOfEach = IntStream.rangeClosed(1, naturalNumbers)
                .map(x -> (int) Math.pow(x, 2))
                .sum();

        int sumOfAll = (int) Math.pow(IntStream.rangeClosed(1, naturalNumbers)
                .map(x -> x)
                .sum(), 2);

        return sumOfAll - sumOfEach;
    }
}
