package com.fabs.kata.problem3;

class App {
    static Long listPrimeNumbersUpTo(long cap) {
        long biggest = 2;

        for (long i = 2; i <= Math.sqrt(cap); i++) {
            if (isPrime(i)) {
                if (cap % i == 0) {
                    biggest = i;
                }
            }
        }

        System.out.println("Biggest factor " + biggest);

        return biggest;
    }

    private static boolean isPrime(long number) {
        if (number == 2) {
            return true;
        }

        double sqrtNumber = Math.sqrt(number);
        for (long i = 2; i <= sqrtNumber; i++) {
            if (number % i == 0) {
                return false;
            }
        }

        return true;
    }
}