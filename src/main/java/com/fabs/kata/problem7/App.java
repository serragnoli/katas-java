package com.fabs.kata.problem7;

import java.util.ArrayList;
import java.util.List;

class App {
    static long getPrimeNumberAtPosition(int nth) {
        List<Long> primes = new ArrayList<>(nth);
        primes.add(2L);

        long counter = 3L;

        while (primes.size() < nth) {
            isPrime(counter++, primes);
        }

        return primes.get(nth-1);
    }

    private static void isPrime(long current, List<Long> primes) {
        for(long i = 2; i <= Math.sqrt(current); i++) {
            if(current % i == 0) {
                return;
            }
        }
        primes.add(current);
    }
}
