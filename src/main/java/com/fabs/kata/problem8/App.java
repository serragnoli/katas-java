package com.fabs.kata.problem8;

import java.util.*;

class App {

    private static final List<Long> DATA_SET = Arrays.asList(
            7L, 3L, 1L, 6L, 7L, 1L, 7L, 6L, 5L, 3L, 1L, 3L, 3L, 0L, 6L, 2L, 4L, 9L, 1L, 9L, 2L, 2L, 5L, 1L, 1L, 9L, 6L, 7L, 4L, 4L, 2L, 6L, 5L, 7L, 4L, 7L, 4L, 2L, 3L, 5L, 5L, 3L, 4L, 9L, 1L, 9L, 4L, 9L, 3L, 4L,
            9L, 6L, 9L, 8L, 3L, 5L, 2L, 0L, 3L, 1L, 2L, 7L, 7L, 4L, 5L, 0L, 6L, 3L, 2L, 6L, 2L, 3L, 9L, 5L, 7L, 8L, 3L, 1L, 8L, 0L, 1L, 6L, 9L, 8L, 4L, 8L, 0L, 1L, 8L, 6L, 9L, 4L, 7L, 8L, 8L, 5L, 1L, 8L, 4L, 3L,
            8L, 5L, 8L, 6L, 1L, 5L, 6L, 0L, 7L, 8L, 9L, 1L, 1L, 2L, 9L, 4L, 9L, 4L, 9L, 5L, 4L, 5L, 9L, 5L, 0L, 1L, 7L, 3L, 7L, 9L, 5L, 8L, 3L, 3L, 1L, 9L, 5L, 2L, 8L, 5L, 3L, 2L, 0L, 8L, 8L, 0L, 5L, 5L, 1L, 1L,
            1L, 2L, 5L, 4L, 0L, 6L, 9L, 8L, 7L, 4L, 7L, 1L, 5L, 8L, 5L, 2L, 3L, 8L, 6L, 3L, 0L, 5L, 0L, 7L, 1L, 5L, 6L, 9L, 3L, 2L, 9L, 0L, 9L, 6L, 3L, 2L, 9L, 5L, 2L, 2L, 7L, 4L, 4L, 3L, 0L, 4L, 3L, 5L, 5L, 7L,
            6L, 6L, 8L, 9L, 6L, 6L, 4L, 8L, 9L, 5L, 0L, 4L, 4L, 5L, 2L, 4L, 4L, 5L, 2L, 3L, 1L, 6L, 1L, 7L, 3L, 1L, 8L, 5L, 6L, 4L, 0L, 3L, 0L, 9L, 8L, 7L, 1L, 1L, 1L, 2L, 1L, 7L, 2L, 2L, 3L, 8L, 3L, 1L, 1L, 3L,
            6L, 2L, 2L, 2L, 9L, 8L, 9L, 3L, 4L, 2L, 3L, 3L, 8L, 0L, 3L, 0L, 8L, 1L, 3L, 5L, 3L, 3L, 6L, 2L, 7L, 6L, 6L, 1L, 4L, 2L, 8L, 2L, 8L, 0L, 6L, 4L, 4L, 4L, 4L, 8L, 6L, 6L, 4L, 5L, 2L, 3L, 8L, 7L, 4L, 9L,
            3L, 0L, 3L, 5L, 8L, 9L, 0L, 7L, 2L, 9L, 6L, 2L, 9L, 0L, 4L, 9L, 1L, 5L, 6L, 0L, 4L, 4L, 0L, 7L, 7L, 2L, 3L, 9L, 0L, 7L, 1L, 3L, 8L, 1L, 0L, 5L, 1L, 5L, 8L, 5L, 9L, 3L, 0L, 7L, 9L, 6L, 0L, 8L, 6L, 6L,
            7L, 0L, 1L, 7L, 2L, 4L, 2L, 7L, 1L, 2L, 1L, 8L, 8L, 3L, 9L, 9L, 8L, 7L, 9L, 7L, 9L, 0L, 8L, 7L, 9L, 2L, 2L, 7L, 4L, 9L, 2L, 1L, 9L, 0L, 1L, 6L, 9L, 9L, 7L, 2L, 0L, 8L, 8L, 8L, 0L, 9L, 3L, 7L, 7L, 6L,
            6L, 5L, 7L, 2L, 7L, 3L, 3L, 3L, 0L, 0L, 1L, 0L, 5L, 3L, 3L, 6L, 7L, 8L, 8L, 1L, 2L, 2L, 0L, 2L, 3L, 5L, 4L, 2L, 1L, 8L, 0L, 9L, 7L, 5L, 1L, 2L, 5L, 4L, 5L, 4L, 0L, 5L, 9L, 4L, 7L, 5L, 2L, 2L, 4L, 3L,
            5L, 2L, 5L, 8L, 4L, 9L, 0L, 7L, 7L, 1L, 1L, 6L, 7L, 0L, 5L, 5L, 6L, 0L, 1L, 3L, 6L, 0L, 4L, 8L, 3L, 9L, 5L, 8L, 6L, 4L, 4L, 6L, 7L, 0L, 6L, 3L, 2L, 4L, 4L, 1L, 5L, 7L, 2L, 2L, 1L, 5L, 5L, 3L, 9L, 7L,
            5L, 3L, 6L, 9L, 7L, 8L, 1L, 7L, 9L, 7L, 7L, 8L, 4L, 6L, 1L, 7L, 4L, 0L, 6L, 4L, 9L, 5L, 5L, 1L, 4L, 9L, 2L, 9L, 0L, 8L, 6L, 2L, 5L, 6L, 9L, 3L, 2L, 1L, 9L, 7L, 8L, 4L, 6L, 8L, 6L, 2L, 2L, 4L, 8L, 2L,
            8L, 3L, 9L, 7L, 2L, 2L, 4L, 1L, 3L, 7L, 5L, 6L, 5L, 7L, 0L, 5L, 6L, 0L, 5L, 7L, 4L, 9L, 0L, 2L, 6L, 1L, 4L, 0L, 7L, 9L, 7L, 2L, 9L, 6L, 8L, 6L, 5L, 2L, 4L, 1L, 4L, 5L, 3L, 5L, 1L, 0L, 0L, 4L, 7L, 4L,
            8L, 2L, 1L, 6L, 6L, 3L, 7L, 0L, 4L, 8L, 4L, 4L, 0L, 3L, 1L, 9L, 9L, 8L, 9L, 0L, 0L, 0L, 8L, 8L, 9L, 5L, 2L, 4L, 3L, 4L, 5L, 0L, 6L, 5L, 8L, 5L, 4L, 1L, 2L, 2L, 7L, 5L, 8L, 8L, 6L, 6L, 6L, 8L, 8L, 1L,
            1L, 6L, 4L, 2L, 7L, 1L, 7L, 1L, 4L, 7L, 9L, 9L, 2L, 4L, 4L, 4L, 2L, 9L, 2L, 8L, 2L, 3L, 0L, 8L, 6L, 3L, 4L, 6L, 5L, 6L, 7L, 4L, 8L, 1L, 3L, 9L, 1L, 9L, 1L, 2L, 3L, 1L, 6L, 2L, 8L, 2L, 4L, 5L, 8L, 6L,
            1L, 7L, 8L, 6L, 6L, 4L, 5L, 8L, 3L, 5L, 9L, 1L, 2L, 4L, 5L, 6L, 6L, 5L, 2L, 9L, 4L, 7L, 6L, 5L, 4L, 5L, 6L, 8L, 2L, 8L, 4L, 8L, 9L, 1L, 2L, 8L, 8L, 3L, 1L, 4L, 2L, 6L, 0L, 7L, 6L, 9L, 0L, 0L, 4L, 2L,
            2L, 4L, 2L, 1L, 9L, 0L, 2L, 2L, 6L, 7L, 1L, 0L, 5L, 5L, 6L, 2L, 6L, 3L, 2L, 1L, 1L, 1L, 1L, 1L, 0L, 9L, 3L, 7L, 0L, 5L, 4L, 4L, 2L, 1L, 7L, 5L, 0L, 6L, 9L, 4L, 1L, 6L, 5L, 8L, 9L, 6L, 0L, 4L, 0L, 8L,
            0L, 7L, 1L, 9L, 8L, 4L, 0L, 3L, 8L, 5L, 0L, 9L, 6L, 2L, 4L, 5L, 5L, 4L, 4L, 4L, 3L, 6L, 2L, 9L, 8L, 1L, 2L, 3L, 0L, 9L, 8L, 7L, 8L, 7L, 9L, 9L, 2L, 7L, 2L, 4L, 4L, 2L, 8L, 4L, 9L, 0L, 9L, 1L, 8L, 8L,
            8L, 4L, 5L, 8L, 0L, 1L, 5L, 6L, 1L, 6L, 6L, 0L, 9L, 7L, 9L, 1L, 9L, 1L, 3L, 3L, 8L, 7L, 5L, 4L, 9L, 9L, 2L, 0L, 0L, 5L, 2L, 4L, 0L, 6L, 3L, 6L, 8L, 9L, 9L, 1L, 2L, 5L, 6L, 0L, 7L, 1L, 7L, 6L, 0L, 6L,
            0L, 5L, 8L, 8L, 6L, 1L, 1L, 6L, 4L, 6L, 7L, 1L, 0L, 9L, 4L, 0L, 5L, 0L, 7L, 7L, 5L, 4L, 1L, 0L, 0L, 2L, 2L, 5L, 6L, 9L, 8L, 3L, 1L, 5L, 5L, 2L, 0L, 0L, 0L, 5L, 5L, 9L, 3L, 5L, 7L, 2L, 9L, 7L, 2L, 5L,
            7L, 1L, 6L, 3L, 6L, 2L, 6L, 9L, 5L, 6L, 1L, 8L, 8L, 2L, 6L, 7L, 0L, 4L, 2L, 8L, 2L, 5L, 2L, 4L, 8L, 3L, 6L, 0L, 0L, 8L, 2L, 3L, 2L, 5L, 7L, 5L, 3L, 0L, 4L, 2L, 0L, 7L, 5L, 2L, 9L, 6L, 3L, 4L, 5L, 0L);

    static Long getGreatestProductOf(int adjacents) {
        NavigableSet<Long> greatest = new TreeSet<>(Comparator.reverseOrder());

        for (int i = 0; i < DATA_SET.size(); i++) {
            int boundary = adjacents + i;
            if (DATA_SET.size() >= boundary) {
                Optional<Long> reduced = DATA_SET.subList(i, boundary).stream()
                        .map(x -> x)
                        .reduce((a, b) -> a * b);

                greatest.add(reduced.orElse(0L));
            }
        }

        return greatest.first();
    }
}
