package com.fabs.kata.problem6;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AppTest {

    @Test
    public void should_resolve_diff_of_square_of_first_10_natural_numbers() {
        int result = App.differenceOfSquaresOfFirst(10);

        assertThat(result).isEqualTo(2640);
    }

    @Test
    public void should_resolve_diff_of_square_of_first_100_natural_numbers() {
        int result = App.differenceOfSquaresOfFirst(100);

        assertThat(result).isEqualTo(25164150);
    }
}
