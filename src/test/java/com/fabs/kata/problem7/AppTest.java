package com.fabs.kata.problem7;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AppTest {

    @Test
    public void should_return_the_6th_prime() {
        long result = App.getPrimeNumberAtPosition(6);

        assertThat(result).isEqualTo(13);
    }

    @Test
    public void should_return_the_100th_prime() {
        long result = App.getPrimeNumberAtPosition(100);

        assertThat(result).isEqualTo(541);
    }

    @Test
    public void should_return_the_1000th_prime() {
        long result = App.getPrimeNumberAtPosition(1000);

        assertThat(result).isEqualTo(7919);
    }

    @Test
    public void should_return_the_5000th_prime() {
        long result = App.getPrimeNumberAtPosition(5000);

        assertThat(result).isEqualTo(48611);
    }

    @Test
    public void should_return_the_10001st_prime() {
        long result = App.getPrimeNumberAtPosition(10001);

        assertThat(result).isEqualTo(104743);
    }
}
