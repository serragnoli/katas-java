package com.fabs.kata.problem5;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


public class AppTest {

    @Test
    public void should_return_the_lcm_of_range_1_to_10() {
        final int result = App.lcmOfRange(10);

        assertThat(result).isEqualTo(2520);
    }

    @Test
    public void should_return_the_lcm_of_range_1_to_20() {
        final int result = App.lcmOfRange(20);

        assertThat(result).isEqualTo(232792560);
    }
}
