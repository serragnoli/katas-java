package com.fabs.kata.problem8;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AppTest {

    @Test
    public void should_return_greatest_product_of_4_adjacent() {
        long result = App.getGreatestProductOf(4);

        assertThat(result).isEqualTo(5832);
    }

    @Test
    public void should_return_greatest_product_of_13_adjacent() {
        long result = App.getGreatestProductOf(13);

        assertThat(result).isEqualTo(23514624000L);
    }
}
