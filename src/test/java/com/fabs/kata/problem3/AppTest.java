package com.fabs.kata.problem3;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


public class AppTest {

    @Test
    public void shoud_return_prime_factors_of_600851475143() {
        final Long result = App.listPrimeNumbersUpTo(600851475143L);

        assertThat(result).isEqualTo(6857L);
    }
}
