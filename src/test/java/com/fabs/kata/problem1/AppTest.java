package com.fabs.kata.problem1;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class AppTest {

    private static final int CAP = 10;
    private static final Integer[] MULTIPLE_OF_3_AND_5_BELOW_10 = new Integer[]{3,5,6,9};

    @Test
    public void should_return_3_5_6_9() {
        List<Integer> multiples = App.listMultiplesOfThreeAndFiveUpTo(CAP);

        assertThat(multiples).containsExactly(MULTIPLE_OF_3_AND_5_BELOW_10);
    }

    @Test
    public void should_sum_multiples_of_cap_10() {
        int result = App.sumOf(CAP);

        assertThat(result).isEqualTo(23);
    }

    @Test
    public void should_sum_multiples_of_cap_1000() {
        int result = App.sumOf(1000);

        assertThat(result).isEqualTo(233168);
    }
}
