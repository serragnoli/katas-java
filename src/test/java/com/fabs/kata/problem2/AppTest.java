package com.fabs.kata.problem2;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;


public class AppTest {

    private static final int TERMS = 10;
    private static final int VALUE_CAP = 100;

    private static final int TERMS_PROBLEM_2 = 33;
    private static final int VALUE_CAP_PROBLEM_2 = 4000000;

    @Test
    public void should_return_fibonacci_values_lower_than_a_million() {
        List<Integer> values = App.fibonacciValuesUpTo(TERMS);

        assertThat(values).containsExactly(1, 2, 3, 5, 8, 13, 21, 34, 55, 89);
    }

    @Test
    public void should_sum_only_even_valued_values_up_to_100() {
        int result = App.sumEvensUpTo(VALUE_CAP, TERMS);

        assertThat(result).isEqualTo(44);
    }

    @Test
    public void should_sum_only_even_valued_values_up_to_4000000() {
        int result = App.sumEvensUpTo(VALUE_CAP_PROBLEM_2, TERMS_PROBLEM_2);

        assertThat(result).isEqualTo(4613732);
    }
}
