package com.fabs.kata.problem4;

import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class AppTest {

    @Test
    public void should_find_the_largest_palindromic_of_product_of_2_digit_numbers() {
        final Optional<Integer> result = App.largestPalindromicOf(2);

        assertThat(result).contains(9009);
    }

    @Test
    public void should_find_the_largest_palindromic_of_product_of_3_digit_numbers() {
        final Optional<Integer> result = App.largestPalindromicOf(3);

        assertThat(result).contains(906609);
    }


}
