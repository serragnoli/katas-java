package com.fabs.kata.problem9;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AppTest {

    @Test
    public void should_find_product_of_pythagorean_triplet_of_12() {
        int result = App.productOfPythagoreanOf(12);

        assertThat(result).isEqualTo(60);
    }

    @Test
    public void should_find_product_of_pythagorean_triplet_of_1000() {
        int result = App.productOfPythagoreanOf(1000);

        assertThat(result).isEqualTo(31875000);
    }
}
